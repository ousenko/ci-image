# CI Image 

This image is based on original Docker-in-Docker image and adds:
- aws cli
- OpenJDK 8 + OpenJ9 

We use it in GitLab CI to 
- build java artifacts with gradle
- build docker images to run these artifacts
- upload these images to ECR
- update ECS tasks and relaunch services 


